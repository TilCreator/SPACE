extends RigidBody2D


export (float) var gravity_radius: float = 3000
export (float) var gravity_acceleration: float = 20
export (bool) var random_texture: bool = true
export (bool) var random_hue: bool = true
export (int) var texture: int = 0
export (float) var hue_shift: float = 0


func _ready() -> void:
    randomize()

    var animations: PoolStringArray = $sprite.frames.get_animation_names()

    if random_texture:
        texture = randi()

    $sprite.animation = animations[texture % animations.size()]

    if random_hue:
        hue_shift = rand_range(0, 1)

    $sprite.set_material($sprite.get_material().duplicate(true))
    $sprite.material.set_shader_param('Shift_Hue', hue_shift)

    gravity_radius *= self.scale.length()


func _physics_process(_delta: float) -> void:
    for moveable_object in get_tree().get_nodes_in_group("moveable_objects"):
        if moveable_object is RigidBody2D:
            var distance = self.global_position.distance_to(moveable_object.global_position)

            if distance < gravity_radius:
                var direction = self.global_position - moveable_object.global_position

                moveable_object.apply_central_impulse(direction.normalized() * (((gravity_radius - distance) / gravity_radius) * gravity_acceleration))
